# Django and PostgreSQL

Project based on Django framework and PostgreSQL.

This project is based in:
- PostgreSQL (local user & pass: admin1 - port:5432)
- Django Architecture

Libraries and others:
- Django==3.2.5
- psycopg2==2.9.9
- Pillow==10.1.0