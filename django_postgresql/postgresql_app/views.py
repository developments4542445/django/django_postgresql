from django.shortcuts import render, get_object_or_404
from .models import DataBaseApp
from django.http import HttpResponse


def index(request):
    databases = DataBaseApp.objects
    return render(request, 'postgresql_app/index.html', {'databases': databases})


def detail(request, postgresql_app_id):
    course_detail = get_object_or_404(DataBaseApp, pk=postgresql_app_id)
    return render(request, 'postgresql_app/detail.html', {'course': course_detail})
